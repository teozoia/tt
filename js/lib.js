function startTick(name,max){
  setInterval(function() {
    var fadeOut = 0;
    if(max >= 0){
      $("#"+name+"").html("");
      $("#"+name+"").html("<p id='"+name+"'>Project: <b>"+name+"</b> ("+max+")</p>");
      if(max == 0){
        $("#"+name+"").html("");
        $("#"+name+"").html("<p id='"+name+"'>Project: <b>"+name+"</b> (finish)</p>");
      }
      max--;
    }
  },1000);
}
